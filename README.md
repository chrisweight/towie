#TOWIE
###(The Only Way is ES6)
-----

####TODO
 - Add Gulp build features
    
-----

####CREDITS:

This source code extends the excellent example from Glen Maddern in his super-easy-to-follow video:

'JavaScript in 2015'
https://www.youtube.com/watch?v=iukBMY4apvI

We re-structure it slightly here to give a better flavour of building a component. 

-----
####SETUP (With package.json and config.js already in place):

    npm install
    jspm install

-----
####SETUP (from scratch):

    npm init

Configuring package.json:
Defaults are mostly fine, for a bit of structure, set 'main' to:

`/app/index.html`

Add the following to the 'scripts' block of your package.json file:

`"serve": "live-server --open=./app/index.html"`

(You'll want this if you're not using http://c9.io or another IDE that gives you a locally running server)

#####Then: 

    npm install live-server --save-dev
    npm install jspm --save-dev
	jspm init

Configuring config.js:
	Defaults are fine.

Add:
	
`separateCSS: true,`

to the inital parameters. 

Then: 

    jspm install npm:jsonp
    jspm install css
    jspm install npm:clean-css --dev

-----
####BUNDLING:  

To create a simple production bundle:
`jspm bundle-sfx --minify app/main app/build.min.js`

or use the 'bundle.sh' script!

-----

