import jsonp from 'jsonp'

class RedditAPI {
    
    constructor() {
        console.info('RedditAPI->constructor()')
        
        // Just a hard-coded URL for ease
        this.redditURL = "https://www.reddit.com/r/cats/search.json?q=cat+gif&restrict_sr=on&sort=relevance&t=all"
    }
    
    load() {
        console.info('RedditAPI->load()')
        
        return new Promise((resolve, reject) => {
            jsonp(this.redditURL, {param: 'jsonp'}, (err,  data) => {
                console.log('RedditAPI->loaded!')
                
               // data.data.children is the json node we're interested in.
               err ? reject(err) : resolve(data.data.children)
            })    
        })
    }
    
}

// This is a simple way of exposing a singleton to an application
export default new RedditAPI()
