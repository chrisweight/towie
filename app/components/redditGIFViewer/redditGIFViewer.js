// We need to use strict here, otherwise we encounter lots of borking.
'use strict'

// Here we're using some JSPM goodness to automate our CSS loading.
import './css/styles.css!'

import RedditAPI from './services/redditAPI'
import ExtractGIFs from './filters/extractGIFs'
import DisplayGIFs from './filters/displayGIFs'

// Look! Extends! Woot! 
class RedditGIFViewer extends HTMLElement {
    
    // When extending an HTMLElement, createdCallback() essentially replaces constuctor(),
    // this is a hole in the spec that is currently being filled
    createdCallback() {
        console.log('RedditGIFViewer->createdCallback()')
        
        RedditAPI
                .load()
                .then(ExtractGIFs)
                .then(DisplayGIFs)
                .then(
                    imgs => 
                        this.innerHTML = `
                            <div id="gifs">${imgs}</div>
                        `
                )
    }
    
}

// Nothing new here, hopefully!
document.registerElement('reddit-gif-viewer', RedditGIFViewer);
