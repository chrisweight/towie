export default (posts) => {
    return posts
        // Handy NSFW filter
        .filter(post => !post.data.over_18)
        .map(post => post.data.url)
        // Reddit likes its GIFV format, so we extract those and lose the 'v' to ensure we're just getting GIFs
        .filter(url => /gifv?$/.exec(url))
        .map(url => url.replace(/v$/, ''))
}