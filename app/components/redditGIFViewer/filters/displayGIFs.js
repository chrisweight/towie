export default (urls) => {
    return urls
            .map(url => `<img src="${url}" >`)
            .join('\n')
}